<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\TransactionDetail;
use App\TravelPackage;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CheckoutController extends Controller
{
    public function index(Request $request, $transaction_id)
    {
        $item = Transaction::with(['user', 'details', 'travel_package'])->findOrFail($transaction_id);
                            
        return view('pages.checkout', ['item' => $item]);
    }

    //memasukkan data ke table transactions. posisinya masih di cart
    public function process(Request $request, $travel_package_id)
    {
        $travel_package = TravelPackage::findOrFail($travel_package_id);

        $transaction = Transaction::create([
            'travel_packages_id' => $travel_package_id,
            'users_id' => Auth::user()->id,
            'additional_visa' => 0,
            'transaction_total' => $travel_package->price,
            'transaction_status' => 'IN_CART'
        ]);

        TransactionDetail::create([
            'transactions_id' => $transaction->id,
            'username' => Auth::user()->username,
            'nationality' => 'ID', //ini kayaknya masih keliru. nationality harusnya ditaruh di tabel user
            'title' => $travel_package->title,
            'is_visa' => false, //ini jg masih keliru
            'doe_passport' => Carbon::now()->addYears(5) //ini jg masih keliru
        ]);

        return redirect()->route('checkout', $transaction->id);
    }

    // menambah user lain yg sudah terdaftar di DB, ke dalam paket travel yg dipilih
    public function create(Request $request, $transaction_id)
    {
        $request->validate([
            'username' => 'required|string|exists:users,username',
            'is_visa' => 'required|boolean',
            'doe_passport' => 'required'
        ]);

        $transaction = Transaction::with(['travel_package'])->find($transaction_id);

        $data = $request->all();
        $data['transactions_id'] = $transaction_id;
        $data['title'] = $transaction->travel_package->title;

        TransactionDetail::create($data);

        // jika is_visa bernilai true
        if($request->is_visa){
            $transaction->transaction_total += 190;
            $transaction->additional_visa += 190;
        }

        $transaction->transaction_total += $transaction->travel_package->price;

        $transaction->save();

        return redirect()->route('checkout', $transaction_id);
    }

    public function remove(Request $request, $detail_id)
    {
        $item = TransactionDetail::findOrFail($detail_id);

        $transaction = Transaction::with(['details', 'travel_package'])->findOrFail($item->transactions_id);

        // jika is_visa bernilai true
        if($item->is_visa){
            $transaction->transaction_total -= 190;
            $transaction->additional_visa -= 190;
        }

        $transaction->transaction_total -= $transaction->travel_package->price;

        $transaction->save();
        $item->delete();

        return redirect()->route('checkout', $item->transactions_id);
    }

    public function success(Request $request, $transaction_id)
    {
        $transaction = Transaction::findOrFail($transaction_id);
        $transaction->transaction_status = 'PENDING';

        $transaction->save();

        return view('pages.success');
    }
}
