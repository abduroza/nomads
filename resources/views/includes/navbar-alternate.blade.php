<div class="container">
    <nav class="row navbar navbar-expand-lg navbar-light bg-white" >
        <div class="navbar-nav mr-auto ml-auto mr-sm-auto mr-lg-0 mr-md-auto">
            <a href="{{route('home')}}" class="navbar-brand"><img src="{{url('frontend/images/logo_nomads.png')}}" alt=""></a>
        </div>
        <!-- d-none d-lg-block -> ketika layar mobile, tidak tampil. ketika layar large(lg) tampil -->
        <ul class="navbar-nav mr-auto d-none d-lg-block">
            <li>
                <span class="text-muted">
                    | &nbsp; Beyond the explorer of the world
                </span>
            </li>
        </ul>
    </nav>
</div>