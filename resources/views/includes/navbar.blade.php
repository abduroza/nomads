<div class="container">
    <nav class="row navbar navbar-expand-lg navbar-light bg-white" >
        <a href="{{route('home')}}" class="navbar-brand">
            <img src="{{url('frontend/images/logo_nomads.png')}}" alt="Logo Nomads">
        </a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navb" >
            <span class="navbar-toggler-icon">

            </span>
        </button>
        <div class="collapse navbar-collapse" id="navb">
            <ul class="navbar-nav ml-auto mr-3">
                <li class="nav-item mx-md-2">
                    <a href="#" class="nav-link active">Home</a>
                </li>
                <li class="nav-item mx-md-2">
                    <a href="#" class="nav-link">Paket Travel</a>
                </li>
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" id="navbardrop" data-toggle="dropdown">Services</a>
                    <div class="dropdown-menu">
                        <a href="#" class="dropdown-item">Link1</a>
                        <a href="#" class="dropdown-item">Link2</a>
                        <a href="#" class="dropdown-item">Link3</a>
                    </div>
                </li>
                <li class="nav-item mx-md-2">
                    <a href="#" class="nav-link">Testimonial</a>
                </li>
            </ul>
            {{-- juka belum login --}}
            @guest
                <!-- mobile button -->
                <!-- d-sm-block => ketika tampilan layar sm(kecil) maka akan menampilkn block. d-md-none => ketika tampilan md(medium) maka tidak akan menampilkn form ini -->
                <form class="form-inline d-sm-block d-md-none">
                    <button class="btn btn-login my-2 my-sm-0" type="button" onclick="event.preventDefault(); location.href='{{ url('login') }}';">
                        Masuk
                    </button>
                </form>
                <!-- desktop button -->
                <!-- form diatur my-2 namun ketika tampilan layar jadi lg(large) maka my dijadikan 0. form tidak tampil, namun ketika tampilan mulai dari md(medium) maka block ditampilkn -->
                <form class="form-inline my-2 my-lg-0 d-none d-md-block">
                    <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4" type="button" onclick="event.preventDefault(); location.href='{{ url('login') }}';">
                        Masuk
                    </button>
                </form>
            @endguest
            {{-- jika sudah login --}}
            @auth
                <!-- mobile button -->
                <!-- d-sm-block => ketika tampilan layar sm(kecil) maka akan menampilkn block. d-md-none => ketika tampilan md(medium) maka tidak akan menampilkn form ini -->
                <form class="form-inline d-sm-block d-md-none" action="{{ url('logout') }}" method="POST">
                    @csrf
                    <button class="btn btn-login my-2 my-sm-0">
                        Keluar
                    </button>
                </form>
                <!-- desktop button -->
                <!-- form diatur my-2 namun ketika tampilan layar jadi lg(large) maka my dijadikan 0. form tidak tampil, namun ketika tampilan mulai dari md(medium) maka block ditampilkn -->
                <form class="form-inline my-2 my-lg-0 d-none d-md-block" action="{{ url('logout') }}" method="POST">
                    @csrf
                    <button class="btn btn-login btn-navbar-right my-2 my-sm-0 px-4">
                        Keluar
                    </button>
                </form>
            @endauth
        </div>
    </nav>
</div>