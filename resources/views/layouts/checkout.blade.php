<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    @stack('prepend-style')
    @include('includes.style')
    {{-- beberapa style tidak semua diload di setiap halaman(biar gak berat). supaya bisa diload dihalaman tertetu saja maka pakai stack dan push --}}
    @stack('addon-style')
</head>
<body>
    @include('includes.navbar-alternate')
    @yield('content')
    @include('includes.footer')
    @stack('prepend-script')
    @include('includes.script')
    {{-- beberapa script tidak semua diload di setiap halaman(biar gak berat). supaya bisa diload dihalaman tertetu saja maka pakai stack dan push --}}
    @stack('addon-script')
</body>
</html>