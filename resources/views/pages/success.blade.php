@extends('layouts.success')

@section('title', 'Checkout Success') {{-- pakai ini lebih singkat. gak perlu @endsection --}}

@section('content')
<main>
    <div class="section-success d-flex align-items-center">
        <div class="col text-center">
            <img src="{{url('frontend/images/ic_mail@2x.png')}}" alt="">
            <h1 class="">Yay! Success</h1>
            <p>
                We've sent you email for trip instructions
                <br>
                Please read it as well
            </p>
            <a href="/" class="btn btn-home-page mt-3 px-5">
                Homepage
            </a>
        </div>
    </div>
</main>
@endsection

