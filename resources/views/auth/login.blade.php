@extends('layouts.login')

@section('content')
    <main class="login-container">
        <div class="container">
            <!-- d-flex berarti memaki flexbox -->
            <div class="row page-login d-flex align-items-center">
                <div class="section-left col-12 col-md-6">
                    <h1>We explore the new <br>life much better</h1>
                    <!-- d-none berarti secara default (layar kecil) tidak muncul gambar. d-sm-flex -> pada layar small akan muncul -->
                    <img src="frontend/images/popular1@2x.jpg" width="400" height="400" alt="" class=" d-none d-sm-flex">
                </div>
                <div class="section-right col-12 col-md-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="text-center">
                                <img src="frontend/images/logo_nomads@2x.png" alt="" class="w-50 mb-4">
                            </div>
                            <form action="{{ route('login') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="email">Email Address</label>
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="form-group form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label for="password" class="form-check-label" for="check">Remember me</label>
                                </div>
                                <button type="submit" class="btn btn-login btn-block">
                                    Sign in
                                </button>
                                <p class="text-center mt-4">
                                    @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        Saya lupa password
                                    </a>
                                    @endif
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
